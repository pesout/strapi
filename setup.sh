# ==================================================== #
#  FUNCTIONS AND GLOBAL VARIABLES
# ==================================================== #

set -e
status () { echo -e "\n${BLUE}➡️  $@${NC}"; }
prompt () { echo -ne "\n${BLUE}$@${NC}"; }
error () { echo -e "\n${RED}Error: ${NC}${1}"; exit 1; }
json() { python3 -c "import sys, json; print(json.load(sys.stdin)['${2}'])" < "$1"; }
slug () { echo $@ | tr "[:blank:]" "-" |  tr '[:upper:]' '[:lower:]'; }

BLUE='\033[1;36m'
YELLOW='\033[1;33m'
RED='\033[1;31m'
NC='\033[0m'

# ==================================================== #
#  APPIO LOGO
# ==================================================== #

echo -e "\n   __ _ _ __  _ __ ( ) ___   ${NC}"
echo -e "  / _\` | '_ \| '_ \| |/ _ \ ${NC}"
echo -e " | (_| | |_) | |_) | | (_) | ${NC}"
echo -e "  \__,_| .__/| .__/|_|\___/  ${NC}"
echo -e "       |_|   |_|             ${NC}"
echo -e "  Strapi setup script 1.0.0  ${NC}"

# ==================================================== #
#  GETTING CONFIGURATION - FROM FILE OR INTERACTIVE
# ==================================================== #

if [ -f "$1" ]; then
    PROJECT_NAME="$(json $1 project_name)"
    NODE_VERSION="$(json $1 node_version)"
    GIT_REPO="$(json $1 git_repo)"
    GIT_BRANCH="$(json $1 git_branch)"
    PORT="$(json $1 port)"
    
    status "Recipe file found"  
else
    status "No valid installation recipe file provided, starting an interactive mode"

    prompt "PROJECT NAME: "
    read PROJECT_NAME
    [ -z "${PROJECT_NAME}" ] && error "Project name is mandatory"

    prompt "NODE VERSION NUMBER [14]: "
    read NODE_VERSION
    NODE_VERSION=${NODE_VERSION:-14}

    prompt "GIT REPOSITORY URL: "
    read GIT_REPO
    [ -z "${GIT_REPO}" ] && error "GIT repository is mandatory"

    prompt "GIT BRANCH [master]: "
    read GIT_BRANCH
    GIT_BRANCH=${GIT_BRANCH:-master}

    prompt "PORT NUMBER [1337]: "
    read PORT
    PORT=${PORT:-1337}
fi

cd "$HOME"
mkdir -p releases
RELEASE_DIR="releases/$(date +%Y%m%d_%H%M%S)"

# ==================================================== #
#  UPDATING PATH and .bash_profile
# ==================================================== #

status "Updating PATH and .bash_profile"

export "PATH=/opt/plesk/node/14/bin:${PATH}"
export "PATH=${HOME}/.yarn/bin:${PATH}"

echo "export PATH=/opt/plesk/node/14/bin:\$PATH" >> "${HOME}/.bash_profile"
echo "export PATH=${HOME}/.yarn/bin:\$PATH" >> "${HOME}/.bash_profile"

# ==================================================== #
#  CLONING GIT REPOSITORY
# ==================================================== #

status "Cloning GIT repository"

git clone -b "${GIT_BRANCH}" --depth 1 "${GIT_REPO}" "${RELEASE_DIR}"

# ==================================================== #
#  UPDATING SERVER CONFIG
# ==================================================== #

status "Updating config/server.js"

sed -i "s/1337/${PORT}/" "${RELEASE_DIR}/config/server.js"

# ==================================================== #
#  INSTALLING STRAPI
# ==================================================== #

status "Installing Strapi"

yarn --cwd "$RELEASE_DIR" install 2> >(grep -v warning 1>&2)
yarn --cwd "$RELEASE_DIR" build

# ==================================================== #
#  INSTALLING pm2
# ==================================================== #

status "Installing pm2"

yarn global add pm2

cp "${HOME}/setup/ecosystem.config.js" "$RELEASE_DIR"

sed -i "s/\[APP_NAME\]/$(slug $PROJECT_NAME)/" "${RELEASE_DIR}/ecosystem.config.js"

cd "$RELEASE_DIR"
pm2 start ecosystem.config.js

pm2 update

# ==================================================== #
#  CREATING SYMBOLIC LINK
# ==================================================== #

status "Creating symlink"

cd "$HOME"
ln -s "${RELEASE_DIR}" current

# ==================================================== #
#  DONE
# ==================================================== #

status "Done!"

# TODO systemctl
